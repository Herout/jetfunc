package jetfunc

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/CloudyKit/jet/v6"
)

func TestAddFunctionsToSet(t *testing.T) {
	dir := "test"
	set := jet.NewSet(jet.NewOSFileSystemLoader(dir), jet.WithSafeWriter(nil))

	// count the lines in test template
	wantedLines := 0
	h, err := os.Open("test/test.jet")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}
	s := bufio.NewScanner(h)
	for s.Scan() {
		wantedLines++
	}
	h.Close()

	// register and prep the test
	AddFunctionsToSet(set)
	template, err := set.GetTemplate("test.jet")
	if err != nil {
		t.Log(err)
		t.FailNow()
	}

	// prep the writer
	var buf bytes.Buffer
	buf.Grow(15000)
	err = template.Execute(&buf, nil, nil)

	// scan for tests
	r := strings.NewReader(buf.String())
	s = bufio.NewScanner(r)
	lineNo := 0
	for s.Scan() {
		lineNo++
		// skip comments and empty lines
		line := strings.TrimSpace(s.Text())
		if line == "" || strings.HasPrefix(line, "//") {
			continue
		}
		// test the template
		got, wanted, err := wantTwoSplinters(line, "==")
		if err != nil {
			t.Log(err)
			t.Fail()
		}
		if got != wanted {
			t.Errorf("line=%d, got=|%s|, wanted=|%s|", lineNo, got, wanted)
		}
	}
	if err = s.Err(); err != nil {
		t.Error(err)
	}
	if lineNo != wantedLines {
		t.Errorf("wanted to see at least %d lines, but processed %d", wantedLines, lineNo)
	}

}

// wantTwoSplinters splits the string, trims both parts, and returns them - or an error.
func wantTwoSplinters(in string, sep string) (p1, p2 string, err error) {
	aux := strings.Split(in, sep)
	if len(aux) != 2 {
		return "", "", fmt.Errorf("wanted 2 parts, got %d", len(aux))
	}
	p1 = strings.TrimSpace(aux[0])
	p2 = strings.TrimSpace(aux[1])
	return p1, p2, err
}
