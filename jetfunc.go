// Package jetfunc provides provides set of functions, which can be used along with the Jet templating engine. It supports jet v6.
// See the templating engine itself, on address https://github.com/CloudyKit/jet.
//
// Usage in templates
//
// All functions can be accessed from template using both camelCase name, or PascalCase name.
// For example, both of these forms will work:
//		{{ leftPad('what',10) }}
//		{{ LeftPad('what',10  }}
//
// Usage in go code
//
//  package main
//  import (
//      "https://github.com/CloudyKit/jet/v6"
//      "gitlab.com/Herout/jetfunc"
//  )
//
//  func main() {
//    dir := "path/to/template/dir"
// 	  set := jet.NewSet(jet.NewOSFileSystemLoader(dir), jet.WithSafeWriter(nil))
//    jetfunc.AddFunctionsToSet(set)
//    // ... and now, do something with the set
//  }
//
// Disclaimer
//
// This is an alpha release. API is unstable. Should this break your code, you get to keep both pieces.
package jetfunc

import (
	"bytes"
	"fmt"
	"math"
	"reflect"
	"strconv"
	"strings"
	"sync"

	"github.com/CloudyKit/jet/v6"
)

const (
	padLeft = iota + 1
	padRight
	padBoth
)

// global stash that is accessile in templates via stashReset, stashSet, and stashGet.
var globalStash struct {
	stash    map[string]string
	canPanic bool // stashGet can panic if key is not in the map
	m        sync.Mutex
}

// AddFunctionsToSet registers all functions to jet.Set.
func AddFunctionsToSet(v *jet.Set) {
	v.AddGlobalFunc("stashReset", StashReset)
	v.AddGlobalFunc("StashReset", StashReset)

	v.AddGlobalFunc("stashSet", StashSet)
	v.AddGlobalFunc("StashSet", StashSet)

	v.AddGlobalFunc("stashGet", StashGet)
	v.AddGlobalFunc("StashGet", StashGet)

	v.AddGlobalFunc("default", Default)
	v.AddGlobalFunc("Default", Default)

	v.AddGlobalFunc("leftPad", LeftPad)
	v.AddGlobalFunc("LeftPad", LeftPad)
	v.AddGlobalFunc("lPad", LeftPad)
	v.AddGlobalFunc("LPad", LeftPad)

	v.AddGlobalFunc("rightPad", RightPad)
	v.AddGlobalFunc("RightPad", RightPad)
	v.AddGlobalFunc("rPad", RightPad)
	v.AddGlobalFunc("RPad", RightPad)

	v.AddGlobalFunc("ifFirst", IfFirst)
	v.AddGlobalFunc("IfFirst", IfFirst)

	v.AddGlobalFunc("QQuote", QQUote)
	v.AddGlobalFunc("qQuote", QQUote)
	v.AddGlobalFunc("qquote", QQUote)

	v.AddGlobalFunc("quote", Quote)
	v.AddGlobalFunc("Quote", Quote)

	v.AddGlobalFunc("trimPrefix", TrimPrefix)
	v.AddGlobalFunc("TrimPrefix", TrimPrefix)

	v.AddGlobalFunc("trimSuffix", TrimSuffix)
	v.AddGlobalFunc("TrimSuffix", TrimSuffix)

	v.AddGlobalFunc("equalFold", EqualFold)
	v.AddGlobalFunc("EqualFold", EqualFold)

	v.AddGlobalFunc("substring", Substring)
	v.AddGlobalFunc("Substring", Substring)

	v.AddGlobalFunc("sqlTag", sqlTag)
	v.AddGlobalFunc("isAmongStrings", IsAmongStrings)
}

// EqualFold is a wrapper over strings.EqualFold.
func EqualFold(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("equalFold", 2, 2)
	x := jetGetParam(a, 0)
	y := jetGetParam(a, 1)
	return reflect.ValueOf(strings.EqualFold(x, y))
}

// InStrings returns true if the first parameter is contained within a set of other strings.
func IsAmongStrings(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("contains", 2, -1)
	want := jetGetParam(a, 0)
	for i:=1;i<a.NumOfArguments();i++ {
		s := jetGetParam(a, 1)
		if strings.EqualFold(want,s) {
			return reflect.ValueOf(true)
		}
	}
	return reflect.ValueOf(false)
}

// Substring returns a substring, based on following three parameters.
//  - what - string, which we want to get substring from.
//  - startAt - integer, starting position from which we want to get substring
//  - wantRunes - optional, integer, number of runes we want to get; if not provided, gets the rest of the string.
// On any error, Substring returns whole "what" parameter.
func Substring(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("substring", 2, 3)
	what := jetGetParam(a, 0)
	startAt, err := jetGetIntParam("substring", a, 1)
	if err != nil {
		return reflect.ValueOf(what)
	}
	wantRunes := len(what)
	if a.NumOfArguments() == 3 {
		wantRunes, err = jetGetIntParam("substring", a, 2)
		if err != nil {
			return reflect.ValueOf(what)
		}
	}

	out := []rune{}
	got := 0
	for i, r := range what {
		if i >= startAt && got < wantRunes {
			got++
			out = append(out, r)
		}
	}
	return reflect.ValueOf(string(out))
}

// TrimPrefix is a wrapper over strings.TrimPrefix.
func TrimPrefix(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("trimPrefix", 2, 2)
	val := jetGetParam(a, 0)
	pfx := jetGetParam(a, 1)
	return reflect.ValueOf(strings.TrimPrefix(val, pfx))
}

// TrimSuffix is a wrapper over strings.TrimSuffix.
func TrimSuffix(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("trimSuffix", 2, 2)
	val := jetGetParam(a, 0)
	pfx := jetGetParam(a, 1)
	return reflect.ValueOf(strings.TrimSuffix(val, pfx))
}

// StashReset creates an empty global stash. The global stash is mutex protected map[string]string, which can
// later on be accessed via StashSet and StashGet functions.
// The function takes one oprional parameter. If the value provided is string "true", then StashGet WILL panic
// if you attempt to access key which was nat yet stored using StashSet.
func StashReset(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("stashReset", 0, 1)
	canPanic := strings.ToLower(jetGetParam(a, 0)) == "true"
	globalStash.m.Lock()
	globalStash.stash = make(map[string]string)
	globalStash.canPanic = canPanic
	globalStash.m.Unlock()
	return reflect.ValueOf("")
}

// StashSet stores a string value in the stash, and returns an empty string.
func StashSet(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("stashSet", 2, 2)
	key := jetGetParam(a, 0)
	val := jetGetParam(a, 1)
	globalStash.m.Lock()
	globalStash.stash[key] = val
	globalStash.m.Unlock()
	return reflect.ValueOf("")
}

// StashGet retrieves string from global stash.
func StashGet(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("stashGet", 1, 1)
	key := jetGetParam(a, 0)
	globalStash.m.Lock()
	val, ok := globalStash.stash[key]
	globalStash.m.Unlock()
	if !ok {
		if globalStash.canPanic {
			a.Panicf("stashGet: key not in stash: %s", key)
		}
	}
	r := reflect.ValueOf(val)
	return r
}

// Default takes one or more parameters, and returns first non-empty string from these parameters, or empty string.
//  {{ default(a,b,c,'return this if all are empty') }}
func Default(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("default", 1, -1)
	for i := 0; i < a.NumOfArguments(); i++ {
		if str := jetGetParam(a, i); str != "" {
			return reflect.ValueOf(str)
		}
	}
	return reflect.ValueOf("")
}

// IfFirst accepts two, or three parameters.
//  {{ ifFirst(aNumber,wantFirst,wantElse)}}
// The function:
//  - returns wantFirst string, if first parameter (aNumber) is integer, and also aNumber == 0
//  - else returns wantElse string; if wantElse is not provided, returns empty string.
func IfFirst(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("ifFirst", 2, 3)
	wantFirst := jetGetParam(a, 1)
	wantElse := ""
	if a.NumOfArguments() == 3 {
		wantElse = jetGetParam(a, 2)
	}
	iIf, err := jetGetIntParam("ifFirst", a, 0)
	if err != nil {
		return reflect.ValueOf(wantElse)
	}
	if iIf == 0 {
		return reflect.ValueOf(wantFirst)
	}
	return reflect.ValueOf(wantElse)
}

// QQUote accepts one, or two parameters.
//  - want - string, will be returned quoted by double qoute, all inside doubleqoutes will be doubled.
//         - ie: {{ doubleQuote('this') }} renders as "this"
//         - ie: {{ doubleQuote('this "is" Sparta') }} renders as "this ""is"" Sparta"
//  - orElse - string, optional; this will be returned 'as is' if want == ""
//           - ie: {{ doubleQuote('','NULL') }} renderes as NULL
func QQUote(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("doubleQuote", 1, 2)
	want := jetGetParam(a, 0)
	want = strings.ReplaceAll(want, `"`, `""`) // double all quotes
	orElse := ""
	if a.NumOfArguments() == 2 {
		orElse = jetGetParam(a, 1)
		if want == "" {
			return reflect.ValueOf(orElse)
		}
	}
	return reflect.ValueOf(`"` + want + `"`)
}

// Quote accepts one, or two parameters.
//  - want - string, will be returned quoted, all inside quotes will be doubled.
//         - ie: {{ doubleQuote('this') }} renders as 'this'
//         - ie: {{ doubleQuote('this ''is'' Sparta') }} renders as 'this ''is'' Sparta'
//  - orElse - string, optional; this will be returned as-is, if want == ""
//           - ie: {{ doubleQuote('','NULL') }} renderes as NULL
func Quote(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("ifFirst", 1, 2)
	want := jetGetParam(a, 0)
	want = strings.ReplaceAll(want, "'", "''") // double all quotes
	orElse := ""
	if a.NumOfArguments() == 2 {
		orElse = jetGetParam(a, 1)
		if want == "" {
			return reflect.ValueOf(orElse)
		}
	}
	return reflect.ValueOf(`'` + want + `'`)
}

// LeftPad returns string padded left by spaces, or by the third parameter, if such is provided.
// Input parametewrs are:
//  - toPad - mandatory string,  which will be padded
//  - toLen - mandatory integer, pad to which length
//  - padChar - optional string, defaults to space
func LeftPad(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("leftPad", 2, 3)
	toPad := jetGetParam(a, 0)
	toLen, err := jetGetIntParam("leftPad", a, 1)
	if err != nil {
		return reflect.ValueOf(toPad)
	}
	padChar := " "
	if a.NumOfArguments() == 3 {
		padChar = jetGetParam(a, 2)
	}
	return reflect.ValueOf(strPad(toPad, toLen, padChar, padLeft))
}

// RightPad returns string padded right by spaces, or by the third parameter, if such is provided.
// Input parametewrs are:
//  - toPad - mandatory string,  which will be padded
//  - toLen - mandatory integer, pad to which length
//  - padChar - optional string, defaults to space
func RightPad(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("rightPad", 2, 3)
	toPad := jetGetParam(a, 0)
	toLen, err := jetGetIntParam("leftPad", a, 1)
	if err != nil {
		return reflect.ValueOf(toPad)
	}
	padChar := " "
	if a.NumOfArguments() == 3 {
		padChar = jetGetParam(a, 2)
	}
	return reflect.ValueOf(strPad(toPad, toLen, padChar, padRight))
}

func strPad(input string, padLength int, padString string, padType int) string {
	var output string

	inputLength := len(input)
	padStringLength := len(padString)

	if inputLength >= padLength {
		return input
	}

	repeat := math.Ceil(float64(1) + (float64(padLength-padStringLength))/float64(padStringLength))

	switch padType {
	case padRight:
		output = input + strings.Repeat(padString, int(repeat))
		output = output[:padLength]
	case padLeft:
		output = strings.Repeat(padString, int(repeat)) + input
		output = output[len(output)-padLength:]
	case padBoth:
		length := (float64(padLength - inputLength)) / float64(2)
		repeat = math.Ceil(length / float64(padStringLength))
		output = strings.Repeat(padString, int(repeat))[:int(math.Floor(float64(length)))] + input + strings.Repeat(padString, int(repeat))[:int(math.Ceil(float64(length)))]
	}

	return output
}

// jetGetParam is a helper function. It converts input parameter to string.
func jetGetParam(a jet.Arguments, i int) string {
	if a.NumOfArguments() <= i {
		return ""
	}
	x := a.Get(i)
	if ! x.IsValid() {
		return ""
	}
	val := bytes.NewBuffer(nil)
	fmt.Fprint(val, x)
	return val.String()
}

// jetGetIntParam is a helper finction, which converts jet function input to from string to int, or returns an error.
func jetGetIntParam(from string, a jet.Arguments, i int) (int, error) {
	if a.NumOfArguments() <= i {
		return 0, fmt.Errorf(from + ": no args provided")
	}
	val := bytes.NewBuffer(nil)
	fmt.Fprint(val, a.Get(i))
	iVal, err := strconv.Atoi(val.String())
	if err != nil {
		return 0, err
	}
	return iVal, nil
}

func sqlTag(a jet.Arguments) reflect.Value {
	a.RequireNumOfArguments("pressTag", 2, 3)
	tag := jetGetParam(a,0)
	this := jetGetParam(a,1)
	single := a.NumOfArguments() < 3

	if !single {
		return reflect.ValueOf(fmt.Sprintf("/*+%s=%s*/", tag, this))
	}
	return reflect.ValueOf(fmt.Sprintf("/*+>%s*/%s/*+<%s*/", tag, this, tag))
}